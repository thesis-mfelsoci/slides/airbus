# Solution of coupled sparse/dense linear systems in an industrial aeroacoustic context

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/airbus/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/airbus/-/commits/master)

[Slides for seminar at Airbus Central R&T](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/airbus/airbus.pdf)

